{ config, lib, pkgs, ... }:

let
  cfg = config.services.erisserve;
  stateDirectoryPath = "\${STATE_DIRECTORY}";
in {

  options.services.erisserve = {
    enable = lib.mkEnableOption "ERIS block server";
    enableGet = lib.mkEnableOption "GET blocks" // { default = true; };
    enablePut = lib.mkEnableOption "PuT blocks";
    listenUrls = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ "coap+tcp://[::1]" ];
      example = [ "coap+tcp://[::1]:5683" "http://[::1]:1337" ];
    };
    storagePath = lib.mkOption {
      type = with lib.types; nullOr str;
      default = stateDirectoryPath;
      description = ''
        Directory for storing blocks.
        If null the service will store blocks in memory.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.erisserve = {
      description = "ERIS block server";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = toString ([ "${pkgs.eriscmd}/bin/eriscmd" "serve" ]
          ++ (lib.optional cfg.enableGet "--get")
          ++ (lib.optional cfg.enablePut "--put")
          ++ (map (x: "--url:${x}") cfg.listenUrls)
          ++ (lib.optional (cfg.storagePath != null)
            "--tkrzw:${cfg.storagePath}/eris.tkh"));
        # I hate systemd and I hate linux.
        DynamicUser = true;
        AmbientCapabilities = "CAP_NET_BIND_SERVICE";
        ReadWritePaths =
          lib.mkIf (cfg.storagePath != stateDirectoryPath) [ cfg.storagePath ];
        StateDirectory =
          lib.mkIf (cfg.storagePath == stateDirectoryPath) "erisserve";
      };
    };
  };

}
