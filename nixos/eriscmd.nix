{ config, lib, pkgs, ... }:

with lib;

let cfg = config.programs.eriscmd;
in {
  options = {

    programs.eriscmd = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Whether to add eriscmd to the global environment.";
      };

      enableLinkHandler = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to configure XDG associations for ERIS link and MIME.
        '';
      };

    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.eriscmd ];
    xdg.mime = mkIf cfg.enableLinkHandler {
      enable = true;
      defaultApplications."application/x-eris-link+cbor" = "eris-open.desktop";
    };
  };
}
