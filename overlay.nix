final: prev:
let
  inherit (final) lib;
  fetchFromCodeberg = { owner ? "eris", repo, rev, hash ? lib.fakeHash }:
    prev.fetchFromGitea {
      domain = "codeberg.org";
      inherit owner repo rev hash;
    };
in {

  eriscmd = final.nimPackages.eris;

  git-annex-backend-XERIS = with final.nimPackages;
    buildNimPackage rec {
      pname = "git-annex-backend-XERIS";
      version = "20230125";
      src = fetchFromCodeberg {
        repo = pname;
        rev = version;
        hash = "sha256-ZPbrqDQcextecMz0ZJX7zgK5jiqzreVMLjJ86y16Pvo=";
      };
      nimBinOnly = true;
      buildInputs = [ eris ];
      postInstall = ''
        mv \
          $out/bin/git_annex_backend_XERIS \
          $out/bin/$pname
      '';
    };

  nimPackages = prev.nimPackages.overrideScope' (final': prev':
    with final'; {

      cbor = let version = "20230310";
      in if lib.strings.versionAtLeast prev'.cbor.version version then
        prev'.cbor
      else
        prev'.cbor.overrideAttrs ({ pname, ... }: {
          inherit version;
          src = prev.fetchFromSourcehut {
            owner = "~ehmry";
            repo = "nim_${pname}";
            rev = version;
            hash = "sha256-VmSYWgXDJLB2D2m3/ymrEytT2iW5JE56WmDz2MPHAqQ=";
          };
        });

      coap = buildNimPackage rec {
        pname = "coap";
        version = "20230125";
        src = fetchFromCodeberg {
          repo = "nim-" + pname;
          rev = version;
          hash = "sha256-wlDyqRxXTrX+zXDIe2o9FTU2o26LO/6m7H/FGok1JDw=";
        };
        propagatedBuildInputs = with final'; [ taps ];
      };

      configparser = buildNimPackage rec {
        pname = "configparser";
        version = "20230120";
        src = prev.fetchFromGitHub {
          repo = "nim-" + pname;
          owner = "ehmry";
          rev = "695f1285d63f1954c25eb1f42798d90fa7bcbe14";
          hash = "sha256-Z2Qr14pv2RHzQNfEYIKuXKHfHvvIfaEiGCHHCWJZFyw=";
        };
        doCheck = true;
      };

      eris = buildNimPackage rec {
        pname = "eris";
        version = "20230716";
        outputs = [ "bin" "out" ];
        src = fetchFromCodeberg {
          repo = "nim-${pname}";
          rev = version;
          hash = "sha256-g2mxua4++zqKeMbe98nBWh7/+rN/IElIiPcvyX0nfEY=";
        };
        propagatedNativeBuildInputs = with prev; [ pkg-config ];
        propagatedBuildInputs = with final'; [
          base32
          coap
          cbor
          freedesktop_org
          illwill
          syndicate
          tkrzw
        ];
        postInstall = ''
          mkdir -p "$bin/share/recoll/filters"
          mv "$bin/bin/rclerislink" "$bin/share/recoll/filters/"

          mkdir -p "$bin/share/applications"
          substitute "eris-open.desktop" "$bin/share/applications/eris-open.desktop"\
            --replace "Exec=eriscmd " "Exec=$bin/bin/eriscmd "

          install -D "eris-link.xml" -t "$bin/share/mime/packages"
          install -D "eris48.png" "$bin/share/icons/hicolor/48x48/apps/eris.png"
        '';
        preCheck = "rm tests/test_syndicate.nim";
        meta = src.meta // {
          license = lib.licenses.unlicense;
          maintainer = with lib.maintainers; [ ehmry ];
          mainProgram = "eriscmd";
        };
      };

      freedesktop_org = buildNimPackage rec {
        pname = "freedesktop_org";
        version = "20230210";
        src = prev.fetchFromSourcehut {
          owner = "~ehmry";
          repo = pname;
          rev = version;
          hash = "sha256-2kgljYwFAnz6p/YyjXwAlPNBzRUnl4v2MyPf0BOoRXI=";
        };
        propagatedBuildInputs = with final'; [ configparser ];
        doCheck = true;
      };

    });

  python3Packages = prev.python3Packages.overrideScope (final': prev':
    with final'; {

      eris = buildPythonPackage rec {
        pname = "eris";
        version = "1.0.0";

        src = fetchFromCodeberg {
          repo = "python-${pname}";
          rev = "v${version}";
          hash = "sha256-ObME7rpTL7Ieft/QIeZFkd4YJND6KqAcURog3bQEh2U=";
        };

        propagatedBuildInputs = [ aiocoap linkheader pycryptodome ];

        doCheck = true;

        meta = src.meta // {
          license = lib.licenses.agpl3Plus;
          maintainer = with lib.maintainers; [ ehmry ];
        };
      };

      linkheader = buildPythonPackage rec {
        pname = "LinkHeader";
        version = "0.4.3";
        src = fetchPypi {
          inherit pname version;
          hash = "sha256-f7vDXAuj+7xTBXHbfhyIbn2z1xiymzRYSKyWhvIbUMM=";
        };
      };

    });

}
