{ pkgs ? import <nixpkgs> { } }:
let
  nimbleSource =
    builtins.fetchGit "https://github.com/nix-community/flake-nimble.git";
  nimbleOverlay = import "${nimbleSource}/overlay.nix";
  erisOverlay = import ./overlay.nix;
  pkgs' = pkgs.appendOverlays [ nimbleOverlay erisOverlay ];
in { inherit (pkgs') eriscmd; }
