# Nix-ERIS

A Nix flake for experimental ERIS packages

## Install
```sh
nix registry add eris "git+https://codeberg.org/eris/nix-eris"
```
