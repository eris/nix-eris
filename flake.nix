{
  description = "ERIS related packages";

  outputs = { self, nixpkgs, syndicate }:
    let
      inherit (nixpkgs) lib;
      overlays' = builtins.attrValues self.overlays;
    in {
      overlays.default = import ./overlay.nix;

      legacyPackages =
        lib.attrsets.mapAttrs (system: pkgs: pkgs.appendOverlays overlays') {
          inherit (nixpkgs.legacyPackages) i686-linux x86_64-linux;
        };

      packages = lib.attrsets.mapAttrs (_: pkgs: {
        inherit (pkgs) eriscmd eris-go git-annex-backend-XERIS;
        python-eris = pkgs.python3Packages.eris;
      }) self.legacyPackages;

      nixosModules = let
        attrs = {
          erisserve = import ./nixos/erisserve.nix;
          eriscmd = import ./nixos/eriscmd.nix;
        };
      in attrs // { default.imports = builtins.attrValues attrs; };

      checks.x86_64-linux = let
        testingPython =
          import "${self.inputs.nixpkgs}/nixos/lib/testing-python.nix";
        test = module:
          testingPython {
            system = "x86_64-linux";
            pkgs = self.legacyPackages.x86_64-linux;
            extraConfigurations = [ { nixpkgs.overlays = overlays'; } module ];
          };
      in {
        erisserve =
          import ./tests/erisserve.nix (test self.nixosModules.erisserve);
        eris-server =
          import ./tests/eris-server.nix (test self.nixosModules.default);
      };

      hydraJobs = self.checks.x86_64-linux // lib.attrsets.mapAttrs
        (_: (lib.attrsets.mapAttrs (_: lib.hydraJob))) {
          inherit (self.packages) i686-linux x86_64-linux;
        };

    };
}
