{ makeTest, pkgs, ... }:

makeTest {
  name = "erisserve";

  nodes.server = {
    environment.systemPackages = with pkgs; [ eriscmd ];
    services.erisserve = {
      enable = true;
      enableGet = true;
      enablePut = true;
    };
  };

  testScript = ''
    start_all()
    server.wait_for_unit("erisserve.service")
    server.wait_for_open_port(5683)
    server.succeed("eriscmd get coap+tcp://[::1] $(echo 'Hail ERIS!' | eriscmd put coap+tcp://[::1])")
  '';
}
