{ makeTest, pkgs, ... }:

makeTest {
  name = "eris-server";

  nodes.server = {
    environment.systemPackages = with pkgs; [ eris-go ];
    programs.eriscmd.enable = true;
    services.eris-server = {
      enable = true;
      decode = true;
      listenHttp = "[::1]:80";
      backends = [ "bolt+file:///tmp/eris.bolt?get&put" ];
      mountpoint = "/eris";
    };
  };

  testScript = ''
    start_all()
    server.wait_for_unit("eris-server.service")
    server.wait_for_open_port(5683)
    server.wait_for_open_port(80)
    server.succeed("eriscmd get http://[::1] $(echo 'Hail ERIS!' | eriscmd put coap+tcp://[::1]:5683)")
  '';
}
